$(document).ready(function() {
	//toggle element x axis
	$(".hseg").on('click', function() {
		$(this).toggleClass("toggleSeg");
		var cls = $(this).attr('class').split(/\s+/)[1];
		update(cls);
	});

	//toggle element y axis
	$(".vseg").on('click', function() {
		$(this).toggleClass("toggleSeg");
		var cls = $(this).attr('class').split(/\s+/)[1];
		update(cls);
	});

	//auto copy to clipboard from textarea
	$("#cpy").on('click', function() {
		this.focus();
		this.select();
		document.execCommand("copy");
		$("#copied").html(" [Copied to clipboard!]");
		setTimeout(function() {
			$("#copied").html("");
		}, 1000);
	});

	//Auto update output from template
	$("#template").on('input', function() {
		updateAll();
	});

	//toggle element y axis
	$("#head").on('input', function() {
		$(":root").get(0).style.setProperty("--main-7seg-color", this.value);
	});

	$("#lower").on('click', function() {
		$(".toggleHighlight").removeClass("toggleHighlight");
		$(".hseg").addClass("toggleSeg");
		$(".vseg").addClass("toggleSeg");
		$("#columns").html(`
			<div class="blob" id="bit0"><div style="float: left;">Bit 0</div><div class="column" draggable="true"><header id="segempt">LEER</header></div></div>
			<div class="blob" id="bit1"><div style="float: left;">Bit 1</div><div class="column" draggable="true"><header id="sege">Seg E</header></div></div>
			<div class="blob" id="bit2"><div style="float: left;">Bit 2</div><div class="column" draggable="true"><header id="segg">Seg G</header></div></div>
			<div class="blob" id="bit3"><div style="float: left;">Bit 3</div><div class="column" draggable="true"><header id="segf">Seg F</header></div></div>
			<div class="blob" id="bit4"><div style="float: left;">Bit 4</div><div class="column" draggable="true"><header id="segd">Seg D</header></div></div>
			<div class="blob" id="bit5"><div style="float: left;">Bit 5</div><div class="column" draggable="true"><header id="segc">Seg C</header></div></div>
			<div class="blob" id="bit6"><div style="float: left;">Bit 6</div><div class="column" draggable="true"><header id="segb">Seg B</header></div></div>
			<div class="blob" id="bit7"><div style="float: left;">Bit 7</div><div class="column" draggable="true"><header id="sega">Seg A</header></div></div>`);
		enableDrag();
		updateAll();
	});

	$("#upper").on('click', function() {
		$(".toggleHighlight").removeClass("toggleHighlight");
		$(".hseg").addClass("toggleSeg");
		$(".vseg").addClass("toggleSeg");
		$("#columns").html(`
			<div class="blob" id="bit0"><div style="float: left;">Bit 0</div><div class="column" draggable="true"><header id="segd">Seg D</header></div></div>
			<div class="blob" id="bit1"><div style="float: left;">Bit 1</div><div class="column" draggable="true"><header id="segc">Seg C</header></div></div>
			<div class="blob" id="bit2"><div style="float: left;">Bit 2</div><div class="column" draggable="true"><header id="segb">Seg B</header></div></div>
			<div class="blob" id="bit3"><div style="float: left;">Bit 3</div><div class="column" draggable="true"><header id="sega">Seg A</header></div></div>
			<div class="blob" id="bit4"><div style="float: left;">Bit 4</div><div class="column" draggable="true"><header id="segempt">LEER</header></div></div>
			<div class="blob" id="bit5"><div style="float: left;">Bit 5</div><div class="column" draggable="true"><header id="sege">Seg E</header></div></div>
			<div class="blob" id="bit6"><div style="float: left;">Bit 6</div><div class="column" draggable="true"><header id="segg">Seg G</header></div></div>
			<div class="blob" id="bit7"><div style="float: left;">Bit 7</div><div class="column" draggable="true"><header id="segf">Seg F</header></div></div>`);
		enableDrag();
		updateAll();
	});

	updateBits();
	enableDrag();
});


/*

Calculating the bits need to be set

*/
function updateBits() {
		var bitstring = "";
		$('#columns').children('div').each(function() {
			if ($(this).find(".toggleHighlight").length > 0) {
				bitstring += "1";
			} else {
				bitstring += "0";
			}
		});
		bitstring = $("#template").val().replace("%bitstring%", bitstring);
		$("#cpy").html(bitstring);
}

function update(cls) {
	if (cls == "segment-a") {
		$("#sega").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-b") {
		$("#segb").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-c") {
		$("#segc").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-d") {
		$("#segd").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-e") {
		$("#sege").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-f") {
		$("#segf").parent().toggleClass("toggleHighlight");
	} else if (cls == "segment-g") {
		$("#segg").parent().toggleClass("toggleHighlight");
	}
	updateBits();
}

function updateAll() {
	if ($("#a").is(".toggleSeg")) {
		$("#sega").parent().removeClass("toggleHighlight");
	} else {
		$("#sega").parent().addClass("toggleHighlight");
	}

	if ($("#b").is(".toggleSeg")) {
		$("#segb").parent().removeClass("toggleHighlight");
	} else {
		$("#segb").parent().addClass("toggleHighlight");
	}

	if ($("#c").is(".toggleSeg")) {
		$("#segc").parent().removeClass("toggleHighlight");
	} else {
		$("#segc").parent().addClass("toggleHighlight");
	}

	if ($("#d").is(".toggleSeg")) {
		$("#segd").parent().removeClass("toggleHighlight");
	} else {
		$("#segd").parent().addClass("toggleHighlight");
	}

	if ($("#e").is(".toggleSeg")) {
		$("#sege").parent().removeClass("toggleHighlight");
	} else {
		$("#sege").parent().addClass("toggleHighlight");
	}

	if ($("#f").is(".toggleSeg")) {
		$("#segf").parent().removeClass("toggleHighlight");
	} else {
		$("#segf").parent().addClass("toggleHighlight");
	}

	if ($("#g").is(".toggleSeg")) {
		$("#segg").parent().removeClass("toggleHighlight");
	} else {
		$("#segg").parent().addClass("toggleHighlight");
	}

	$("#segempt").parent().removeClass("toggleHighlight");

	updateBits();
}

/*

Dragging of elements

*/

var dragSrcEl = null;
function handleDragStart(e) {
	// Target (this) element is the source node.
	dragSrcEl = this;

	e.dataTransfer.effectAllowed = 'move';
	e.dataTransfer.setData('text/html', this.innerHTML);
}

function enableDrag(){
	var cols = document.querySelectorAll('#columns .column');
	[].forEach.call(cols, function(col) {
		col.addEventListener('dragstart', handleDragStart, false);
		col.addEventListener('dragenter', handleDragEnter, false)
		col.addEventListener('dragover', handleDragOver, false);
		col.addEventListener('dragleave', handleDragLeave, false);
		col.addEventListener('drop', handleDrop, false);
		col.addEventListener('dragend', handleDragEnd, false);
	});
}

function handleDragOver(e) {
	if (e.preventDefault) {
		e.preventDefault(); // Necessary. Allows us to drop.
	}

	e.dataTransfer.dropEffect = 'move'; // See the section on the DataTransfer object.

	return false;
}

function handleDragEnter(e) {
	// this / e.target is the current hover target.
	this.classList.add('over');
}

function handleDragLeave(e) {
	this.classList.remove('over'); // this / e.target is previous target element.
}

function handleDrop(e) {
	// this/e.target is current target element.

	if (e.stopPropagation) {
		e.stopPropagation(); // Stops some browsers from redirecting.
	}

	// Don't do anything if dropping the same column we're dragging.
	if (dragSrcEl != this) {
		// Set the source column's HTML to the HTML of the columnwe dropped on.
		dragSrcEl.innerHTML = this.innerHTML;
		this.innerHTML = e.dataTransfer.getData('text/html');
	}
	updateAll();
	return false;
}

function handleDragEnd(e) {
	// this/e.target is the source node.
	var cols = document.querySelectorAll('#columns .column');
	[].forEach.call(cols, function(col) {
		col.classList.remove('over');
	});
}
